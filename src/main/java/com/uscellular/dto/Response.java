
package com.uscellular.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * General response details.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "responseCode",
    "responseDescription"
})
public class Response {

    /**
     * Response Code
     * (Required)
     * 
     */
    @JsonProperty("responseCode")
    @JsonPropertyDescription("Response Code")
    private String responseCode;
    /**
     * Response Code Description
     * (Required)
     * 
     */
    @JsonProperty("responseDescription")
    @JsonPropertyDescription("Response Code Description")
    private String responseDescription;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Response() {
    }

    /**
     * 
     * @param responseDescription
     * @param responseCode
     */
    public Response(String responseCode, String responseDescription) {
        super();
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
    }

    /**
     * Response Code
     * (Required)
     * 
     */
    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Response Code
     * (Required)
     * 
     */
    @JsonProperty("responseCode")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Response Code Description
     * (Required)
     * 
     */
    @JsonProperty("responseDescription")
    public String getResponseDescription() {
        return responseDescription;
    }

    /**
     * Response Code Description
     * (Required)
     * 
     */
    @JsonProperty("responseDescription")
    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

	@Override
	public String toString() {
		return "Response [responseCode=" + responseCode + ", responseDescription=" + responseDescription + "]";
	}
    
    

}
