
package com.uscellular.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Message Tracking Information.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "trackingId",
    "requestTimestamp",
    "responseTimestamp"
})
public class TrackingInfo {

    /**
     * tracking Id of the transaction.
     * (Required)
     * 
     */
    @JsonProperty("trackingId")
    @JsonPropertyDescription("tracking Id of the transaction.")
    private String trackingId;
    /**
     * Timestamp when the request is received from Consumer.
     * (Required)
     * 
     */
    @JsonProperty("requestTimestamp")
    @JsonPropertyDescription("Timestamp when the request is received from Consumer.")
    private String requestTimestamp;
    /**
     * Timestamp when the response is sent to Consumer.
     * (Required)
     * 
     */
    @JsonProperty("responseTimestamp")
    @JsonPropertyDescription("Timestamp when the response is sent to Consumer.")
    private String responseTimestamp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TrackingInfo() {
    }

    /**
     * 
     * @param requestTimestamp
     * @param responseTimestamp
     * @param trackingId
     */
    public TrackingInfo(String trackingId, String requestTimestamp, String responseTimestamp) {
        super();
        this.trackingId = trackingId;
        this.requestTimestamp = requestTimestamp;
        this.responseTimestamp = responseTimestamp;
    }

    /**
     * tracking Id of the transaction.
     * (Required)
     * 
     */
    @JsonProperty("trackingId")
    public String getTrackingId() {
        return trackingId;
    }

    /**
     * tracking Id of the transaction.
     * (Required)
     * 
     */
    @JsonProperty("trackingId")
    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    /**
     * Timestamp when the request is received from Consumer.
     * (Required)
     * 
     */
    @JsonProperty("requestTimestamp")
    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    /**
     * Timestamp when the request is received from Consumer.
     * (Required)
     * 
     */
    @JsonProperty("requestTimestamp")
    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    /**
     * Timestamp when the response is sent to Consumer.
     * (Required)
     * 
     */
    @JsonProperty("responseTimestamp")
    public String getResponseTimestamp() {
        return responseTimestamp;
    }

    /**
     * Timestamp when the response is sent to Consumer.
     * (Required)
     * 
     */
    @JsonProperty("responseTimestamp")
    public void setResponseTimestamp(String responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

	@Override
	public String toString() {
		return "TrackingInfo [trackingId=" + trackingId + ", requestTimestamp=" + requestTimestamp
				+ ", responseTimestamp=" + responseTimestamp + "]";
	}
    
    

}
