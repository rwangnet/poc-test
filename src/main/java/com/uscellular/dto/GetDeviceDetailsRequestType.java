
package com.uscellular.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "meid",
    "transactionId"
})
public class GetDeviceDetailsRequestType {

    /**
     * DecMEID number i.e IMEI number converted to 18digits.
     * (Required)
     * 
     */
    @JsonProperty("meid")
    @JsonPropertyDescription("DecMEID number i.e IMEI number converted to 18digits.")
    private String meid;
    /**
     * Unique transaction Id to track end to end.
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    @JsonPropertyDescription("Unique transaction Id to track end to end.")
    private String transactionId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GetDeviceDetailsRequestType() {
    }

    /**
     * 
     * @param meid
     * @param transactionId
     */
    public GetDeviceDetailsRequestType(String meid, String transactionId) {
        super();
        this.meid = meid;
        this.transactionId = transactionId;
    }

    /**
     * DecMEID number i.e IMEI number converted to 18digits.
     * (Required)
     * 
     */
    @JsonProperty("meid")
    public String getMeid() {
        return meid;
    }

    /**
     * DecMEID number i.e IMEI number converted to 18digits.
     * (Required)
     * 
     */
    @JsonProperty("meid")
    public void setMeid(String meid) {
        this.meid = meid;
    }

    /**
     * Unique transaction Id to track end to end.
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Unique transaction Id to track end to end.
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

	@Override
	public String toString() {
		return "GetDeviceDetailsRequestType [meid=" + meid + ", transactionId=" + transactionId + "]";
	}
    
    

}
