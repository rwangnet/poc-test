
package com.uscellular.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "trackingInfo",
    "response",
    "deviceDetails"
})
public class GetDeviceDetailsResponseType {

    /**
     * Message Tracking Information.
     * (Required)
     * 
     */
    @JsonProperty("trackingInfo")
    @JsonPropertyDescription("Message Tracking Information.")
    private TrackingInfo trackingInfo;
    /**
     * General response details.
     * (Required)
     * 
     */
    @JsonProperty("response")
    @JsonPropertyDescription("General response details.")
    private Response response;
    /**
     * Device Details.
     * 
     */
    @JsonProperty("deviceDetails")
    @JsonPropertyDescription("Device Details.")
    private DeviceDetails deviceDetails;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GetDeviceDetailsResponseType() {
    }

    /**
     * 
     * @param deviceDetails
     * @param trackingInfo
     * @param response
     */
    public GetDeviceDetailsResponseType(TrackingInfo trackingInfo, Response response, DeviceDetails deviceDetails) {
        super();
        this.trackingInfo = trackingInfo;
        this.response = response;
        this.deviceDetails = deviceDetails;
    }

    /**
     * Message Tracking Information.
     * (Required)
     * 
     */
    @JsonProperty("trackingInfo")
    public TrackingInfo getTrackingInfo() {
        return trackingInfo;
    }

    /**
     * Message Tracking Information.
     * (Required)
     * 
     */
    @JsonProperty("trackingInfo")
    public void setTrackingInfo(TrackingInfo trackingInfo) {
        this.trackingInfo = trackingInfo;
    }

    /**
     * General response details.
     * (Required)
     * 
     */
    @JsonProperty("response")
    public Response getResponse() {
        return response;
    }

    /**
     * General response details.
     * (Required)
     * 
     */
    @JsonProperty("response")
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * Device Details.
     * 
     */
    @JsonProperty("deviceDetails")
    public DeviceDetails getDeviceDetails() {
        return deviceDetails;
    }

    /**
     * Device Details.
     * 
     */
    @JsonProperty("deviceDetails")
    public void setDeviceDetails(DeviceDetails deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

	@Override
	public String toString() {
		return "GetDeviceDetailsResponseType [trackingInfo=" + trackingInfo + ", response=" + response
				+ ", deviceDetails=" + deviceDetails + "]";
	}
    
    

}
