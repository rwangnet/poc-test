
package com.uscellular.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Device Details.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "blacklistInd",
    "make",
    "model",
    "itemId",
    "itemDescription",
    "transactionId",
    "meid"
})
public class DeviceDetails {

    /**
     * Black List Indicator
     * (Required)
     * 
     */
    @JsonProperty("blacklistInd")
    @JsonPropertyDescription("Black List Indicator")
    private DeviceDetails.BlacklistInd blacklistInd;
    /**
     * Device make
     * (Required)
     * 
     */
    @JsonProperty("make")
    @JsonPropertyDescription("Device make")
    private String make;
    /**
     * Device model
     * (Required)
     * 
     */
    @JsonProperty("model")
    @JsonPropertyDescription("Device model")
    private String model;
    /**
     * Device ItemId
     * (Required)
     * 
     */
    @JsonProperty("itemId")
    @JsonPropertyDescription("Device ItemId")
    private String itemId;
    /**
     * Device Item Description
     * (Required)
     * 
     */
    @JsonProperty("itemDescription")
    @JsonPropertyDescription("Device Item Description")
    private String itemDescription;
    /**
     * Unique transactionId to track end to end
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    @JsonPropertyDescription("Unique transactionId to track end to end")
    private String transactionId;
    /**
     * Decimal MEID or serialNumber which was converted using IMEI
     * (Required)
     * 
     */
    @JsonProperty("meid")
    @JsonPropertyDescription("Decimal MEID or serialNumber which was converted using IMEI")
    private String meid;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DeviceDetails() {
    }

    /**
     * 
     * @param meid
     * @param itemId
     * @param model
     * @param itemDescription
     * @param make
     * @param transactionId
     * @param blacklistInd
     */
    public DeviceDetails(DeviceDetails.BlacklistInd blacklistInd, String make, String model, String itemId, String itemDescription, String transactionId, String meid) {
        super();
        this.blacklistInd = blacklistInd;
        this.make = make;
        this.model = model;
        this.itemId = itemId;
        this.itemDescription = itemDescription;
        this.transactionId = transactionId;
        this.meid = meid;
    }

    /**
     * Black List Indicator
     * (Required)
     * 
     */
    @JsonProperty("blacklistInd")
    public DeviceDetails.BlacklistInd getBlacklistInd() {
        return blacklistInd;
    }

    /**
     * Black List Indicator
     * (Required)
     * 
     */
    @JsonProperty("blacklistInd")
    public void setBlacklistInd(DeviceDetails.BlacklistInd blacklistInd) {
        this.blacklistInd = blacklistInd;
    }

    /**
     * Device make
     * (Required)
     * 
     */
    @JsonProperty("make")
    public String getMake() {
        return make;
    }

    /**
     * Device make
     * (Required)
     * 
     */
    @JsonProperty("make")
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * Device model
     * (Required)
     * 
     */
    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    /**
     * Device model
     * (Required)
     * 
     */
    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Device ItemId
     * (Required)
     * 
     */
    @JsonProperty("itemId")
    public String getItemId() {
        return itemId;
    }

    /**
     * Device ItemId
     * (Required)
     * 
     */
    @JsonProperty("itemId")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Device Item Description
     * (Required)
     * 
     */
    @JsonProperty("itemDescription")
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * Device Item Description
     * (Required)
     * 
     */
    @JsonProperty("itemDescription")
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * Unique transactionId to track end to end
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Unique transactionId to track end to end
     * (Required)
     * 
     */
    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Decimal MEID or serialNumber which was converted using IMEI
     * (Required)
     * 
     */
    @JsonProperty("meid")
    public String getMeid() {
        return meid;
    }

    /**
     * Decimal MEID or serialNumber which was converted using IMEI
     * (Required)
     * 
     */
    @JsonProperty("meid")
    public void setMeid(String meid) {
        this.meid = meid;
    }

    public enum BlacklistInd {

        Y("Y"),
        N("N");
        private final String value;
        private final static Map<String, DeviceDetails.BlacklistInd> CONSTANTS = new HashMap<String, DeviceDetails.BlacklistInd>();

        static {
            for (DeviceDetails.BlacklistInd c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private BlacklistInd(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static DeviceDetails.BlacklistInd fromValue(String value) {
            DeviceDetails.BlacklistInd constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

	@Override
	public String toString() {
		return "DeviceDetails [blacklistInd=" + blacklistInd + ", make=" + make + ", model=" + model + ", itemId="
				+ itemId + ", itemDescription=" + itemDescription + ", transactionId=" + transactionId + ", meid="
				+ meid + "]";
	}
    
    

}
