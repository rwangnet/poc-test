
package org.xmlsoap.schemas.soap.envelope_1588617079356;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import tdscorp.teldta.com.boss.in.blacklist_1588617079356.MtBlackListCheckReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="Body">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://com.teldta.tdscorp/boss/in/blacklist}mtBlackListCheckReq"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "body"
})
@XmlRootElement(name = "Envelope")
public class Envelope {

    @XmlElement(name = "Header", required = true)
    protected Object header;
    @XmlElement(name = "Body", required = true)
    protected Envelope.Body body;

    /**
     * Obtiene el valor de la propiedad header.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getHeader() {
        return header;
    }

    /**
     * Define el valor de la propiedad header.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setHeader(Object value) {
        this.header = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     * 
     * @return
     *     possible object is
     *     {@link Envelope.Body }
     *     
     */
    public Envelope.Body getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     * 
     * @param value
     *     allowed object is
     *     {@link Envelope.Body }
     *     
     */
    public void setBody(Envelope.Body value) {
        this.body = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://com.teldta.tdscorp/boss/in/blacklist}mtBlackListCheckReq"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mtBlackListCheckReq"
    })
    public static class Body {

        @XmlElement(namespace = "http://com.teldta.tdscorp/boss/in/blacklist", required = true)
        protected MtBlackListCheckReq mtBlackListCheckReq;

        /**
         * Obtiene el valor de la propiedad mtBlackListCheckReq.
         * 
         * @return
         *     possible object is
         *     {@link MtBlackListCheckReq }
         *     
         */
        public MtBlackListCheckReq getMtBlackListCheckReq() {
            return mtBlackListCheckReq;
        }

        /**
         * Define el valor de la propiedad mtBlackListCheckReq.
         * 
         * @param value
         *     allowed object is
         *     {@link MtBlackListCheckReq }
         *     
         */
        public void setMtBlackListCheckReq(MtBlackListCheckReq value) {
            this.mtBlackListCheckReq = value;
        }

    }

}
