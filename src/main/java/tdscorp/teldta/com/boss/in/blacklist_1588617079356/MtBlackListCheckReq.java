
package tdscorp.teldta.com.boss.in.blacklist_1588617079356;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}BOSSTransId"/>
 *         &lt;element ref="{}SerialNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bossTransId",
    "serialNumber"
})
@XmlRootElement(name = "mtBlackListCheckReq")
public class MtBlackListCheckReq {

    @XmlElement(name = "BOSSTransId")
    @XmlSchemaType(name = "unsignedInt")
    protected long bossTransId;
    @XmlElement(name = "SerialNumber", required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger serialNumber;

    /**
     * Obtiene el valor de la propiedad bossTransId.
     * 
     */
    public long getBOSSTransId() {
        return bossTransId;
    }

    /**
     * Define el valor de la propiedad bossTransId.
     * 
     */
    public void setBOSSTransId(long value) {
        this.bossTransId = value;
    }

    /**
     * Obtiene el valor de la propiedad serialNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    /**
     * Define el valor de la propiedad serialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSerialNumber(BigInteger value) {
        this.serialNumber = value;
    }

}
