
package request;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "trackingId",
    "requestTimestamp",
    "responseTimestamp"
})
public class TrackingInfo {

    @JsonProperty("trackingId")
    private String trackingId;
    @JsonProperty("requestTimestamp")
    private String requestTimestamp;
    @JsonProperty("responseTimestamp")
    private String responseTimestamp;

    /**
     * 
     * @return
     *     The trackingId
     */
    @JsonProperty("trackingId")
    public String getTrackingId() {
        return trackingId;
    }

    /**
     * 
     * @param trackingId
     *     The trackingId
     */
    @JsonProperty("trackingId")
    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    /**
     * 
     * @return
     *     The requestTimestamp
     */
    @JsonProperty("requestTimestamp")
    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    /**
     * 
     * @param requestTimestamp
     *     The requestTimestamp
     */
    @JsonProperty("requestTimestamp")
    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    /**
     * 
     * @return
     *     The responseTimestamp
     */
    @JsonProperty("responseTimestamp")
    public String getResponseTimestamp() {
        return responseTimestamp;
    }

    /**
     * 
     * @param responseTimestamp
     *     The responseTimestamp
     */
    @JsonProperty("responseTimestamp")
    public void setResponseTimestamp(String responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

}
