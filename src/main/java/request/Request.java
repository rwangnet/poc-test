
package request;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "trackingInfo"
})
public class Request {

    @JsonProperty("trackingInfo")
    private TrackingInfo trackingInfo;

    /**
     * 
     * @return
     *     The trackingInfo
     */
    @JsonProperty("trackingInfo")
    public TrackingInfo getTrackingInfo() {
        return trackingInfo;
    }

    /**
     * 
     * @param trackingInfo
     *     The trackingInfo
     */
    @JsonProperty("trackingInfo")
    public void setTrackingInfo(TrackingInfo trackingInfo) {
        this.trackingInfo = trackingInfo;
    }

}
